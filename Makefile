
.PHONY: all clean run

MAIN_PROGRAM = root.byte
SRC_FILES    = $(wildcard *.ml) $(wildcard view/*.ml) $(wildcard controller/*.ml) $(wildcard model/*.ml) $(wildcard middleware/*.ml)
BUILD        = corebuild
PKGS         = opium,cow,cow.syntax,uri,base64,mongo,safepass,cohttp
BUILD_OPT    = -use-ocamlfind -pkgs $(PKGS)

all: $(MAIN_PROGRAM)

clean:
	$(BUILD) -clean

run: $(MAIN_PROGRAM)
	./$(MAIN_PROGRAM) -p 9000 -v -d

$(MAIN_PROGRAM): $(SRC_FILES)
	$(BUILD) -I middleware -I model -I view -I controller $(BUILD_OPT) $(MAIN_PROGRAM)
