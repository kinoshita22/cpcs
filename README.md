Competitive Proving Contest System

# Setup #

```
$ git clone https://bitbucket.org/kinoshita22/cpcs.git
$ opam install opium cow uri base64 mongo safepass cohttp
$ # edit config_example.ml and rename it to config.ml
$ make run
```