open Core.Std

let agda_exec = Config.agda_exec
let agda_stdlib = Config.agda_stdlib

let random_hex len =
  Random.self_init ();
  if len < 0 then invalid_arg "len must be positive";
  let rec loc s = function
    | 0 -> s
    | n -> loc (Printf.sprintf "%x%s" (Random.int 16) s) (n - 1)
  in
  loc "" len

let check ?(definitions="") ~verifier ~solution =
  Printf.sprintf "build.ml: %s %s %s" definitions verifier solution
  |> print_endline;

  let dir = Filename.temp_dir ~perm:0o700 "cpcs" "" in
  let solution_path    = Filename.concat dir "Solution.agda"
  and definitions_path = Filename.concat dir "Definitions.agda"
  and verifier_path    = Filename.concat dir "Verifier.agda"
  in
  Out_channel.write_all verifier_path    verifier;
  Out_channel.write_all definitions_path definitions;
  Out_channel.write_all solution_path    solution;

  let cmd =
    Printf.sprintf "%s --safe -i %s -i %s %s"
      agda_exec
      agda_stdlib
      dir
      verifier_path
  in

  let inc = Unix.open_process_in cmd in
  let res = In_channel.input_lines inc (* WARNING: directory names are revealed *)
  in
  let module R = Result in
  match Unix.close_process_in inc with
  | R.Ok    _ -> R.Ok ()
  | R.Error _ -> R.Error res
