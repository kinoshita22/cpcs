
let database : string = "cpcs" (* database name *)

let agda_exec : string = "agda" (* path to agda executable *)

let agda_stdlib : string = "agda-stdlib" (* path to agda stdlib *)
