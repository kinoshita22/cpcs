open Core.Std
open Misc
module O = Opium.Std

let logout = O.get "/logout" begin fun req ->
  Session.destroy req;
  O.redirect' (Uri.of_string "/")
end

let show_signup = O.get "/signup" begin fun _ ->
  Signup_view.page ()
  |> return_html ~title:"Signup" ~login:None
end

let register = post_with_query "/signup" begin fun req query ->
  let username = extract_s query "username" |> String.lowercase
  and password = extract_s query "password"
  in
  match User_model.validate username password with
  | Error msg ->
    Signup_view.page ~username ~message:(General.error msg) ()
    |> return_html ~title:"Signup" ~login:None
  | Ok _ ->
    User_model.insert username password;
    O.redirect (Uri.of_string "/")
    |> Session.set req ~key:"login" ~data:username
    |> O.return
end

let show_login = O.get "/login" begin fun _ ->
  Login_view.page ()
  |> return_html ~title:"Login" ~login:None
end

let authenticate = post_with_query "/login" begin fun req query ->
  let username = extract_s query "username"
  and password = extract_s query "password"
  in
  if User_model.authenticate username password
  then
    O.redirect (Uri.of_string "/")
    |> Session.set req ~key:"login" ~data:username
    |> O.return
  else
    Login_view.page
      ~username
      ~message:(General.error "Username or password is incorrect.")
      ()
    |> return_html ~title:"Login" ~login:None
end

let c = cascade [logout; show_signup; register; show_login; authenticate]
