open Core.Std
open Misc
module O = Opium.Std

let (>>=) = O.(>>=)

let page = O.get "/contest/" begin fun req ->
  let login = Session.get req ~key:"login" in
  Contest_view.page ~login
  |> return_html ~title:"Contest" ~login
end

let organize_page = O.get "/contest/organize" begin fun req ->
  Contest_view.organize ()
  |> return_html ~title:"Organize" ~login:(Session.get req ~key:"login")
end

let organize = post_with_query "/contest/organize" begin fun req query ->
  let login    = Session.get req ~key:"login" in
  let name     = extract_s query "name"
  and start    = extract_s query "start"
  and finish   = extract_s query "finish"
  and problems = extract_s query "problems"
  in
  begin
    match Contest_model.validate ~login ~name ~start ~finish ~problems with
    | Error msg ->
      Contest_view.organize ~name ~start ~finish ~problems
        ~message:(General.error msg) ()
    | Ok _ ->
      Contest_model.insert
        ~name
        ~organized_by:(Option.value_exn login)
        ~start:(Time.of_string start)
        ~finish:(Time.of_string finish)
        ~problems;
      Create_view.ok
  end
  |> return_html ~title:"Organize" ~login

end

let show = O.get "/contest/:id/" begin fun req ->
  match Contest_model.get_current_by_id (O.param req "id") with
  | None   -> Lwt.fail (Client_error `Not_found)
  | Some c ->
    Contest_view.show c
    |> return_html ~title:"Contest" ~login:(Session.get req "login")
end

let problem = O.get "/contest/:id/:problem_id" begin fun req ->
  match Contest_model.get_current_by_id (O.param req "id") with
  | None -> Lwt.fail (Client_error `Not_found)
  | Some { problems; _ } ->
    begin
      match List.find ~f:(fun { id; _} -> id = O.param req "problem_id") problems with
      | None -> Lwt.fail (Client_error `Not_found)
      | Some problem ->
        let login = Session.get req "login" in
        Problem_view.page problem ~login
        |> return_html ~title:"Contest" ~login
    end
end

let solve = post_with_query "/contest/:id/:problem_id" begin fun req query ->
  match Contest_model.verify (O.param req "id") (O.param req "problem_id") with
  | None -> Lwt.fail (Client_error `Not_found)
  | Some (contest, ({ Problem_model.id; _ } as problem)) ->
    let login = Session.get req ~key:"login" in
    if Option.is_none login
    then
      Problem_view.page
        ~message:(General.error "Login required to solve problems in contest.")
        problem
        ~login
      |> return_html ~title:"Contest" ~login
    else
      let solution = extract_s query "solution" in
      begin
        match Problem_model.check_solution ~id ~solution with
        | Result.Ok _ ->
          Solution_model.insert ~proof:solution ~problem_id:id ~login;
          Problem_model.verify ~id;
          Problem_view.ok
        | Result.Error err -> General.build_error (String.concat ~sep:"\n" err)
      end
      |> return_html ~title:"Contest" ~login
end
 
let c = cascade [page; organize_page; organize; show; problem; solve]
