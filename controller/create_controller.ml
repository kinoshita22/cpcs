(* Problem creation page. *)

open Core.Std
open Misc
module O = Opium.Std

let create_page = O.get "/create" begin fun req ->
  Create_view.page ()
  |> Base.html ~title:"Create" ~login:(Session.get req ~key:"login")
  |> return_html
end

let create_problem = post_with_query "/create" begin fun req query ->

  let title       = extract_s query "title"
  and description = extract_s query "description"
  and definitions = extract_s query "definitions"
  and verifier    = extract_s query "verifier"
  and solution    = extract_s query "solution"
  and public      = extract_s query "public" = "on"
  and user        = Option.value (Session.get req ~key:"login") ~default:""
  and verified    = ref false
  in

  let res =
    Problem_model.validate
      ~title ~definitions ~verifier ~created_by:user ~solution
  in
  begin
    match res with
    | Ok _ ->
      Problem_model.create
        ~title ~description ~definitions
        ~verifier ~created_by:user ~public
        ~verified:(!verified);
      Create_view.ok
      |> Base.html ~title:"Create" ~login:(Session.get req ~key:"login")
    | Error message ->
      Create_view.page
        ~title ~description ~definitions ~verifier ~solution ~public ~message ()
      |> Base.html ~title:"Create" ~login:(Session.get req ~key:"login")
  end
  |> return_html

end
