open Core.Std
open Misc
module O = Opium.Std

let edit_problem_get = O.get "/edit/:id" begin fun req ->
  let login = Session.get req ~key:"login" in
  let { Problem_model.title
      ; description
      ; definitions
      ; verifier
      ; created_by
      ; public
      ; _ } = Problem_model.validate_edit ~id:(O.param req "id") ~login
  in
  Create_view.page
    ~title ~description ~definitions ~verifier ~public ()
  |> Base.html ~title:"Edit" ~login
  |> return_html
end

let edit_problem_post = post_with_query "/edit/:id" begin fun req query ->

  let login       = Session.get req "login"
  and title       = extract_s query "title"
  and description = extract_s query "description"
  and definitions = extract_s query "definitions"
  and verifier    = extract_s query "verifier"
  and solution    = extract_s query "solution"
  and public      = extract_s query "public" = "on"
  in
  
  let res =
    Problem_model.edit
      ~id:(O.param req "id")
      ~login ~title ~description ~definitions
      ~verifier ~solution ~public
  in
  begin
    match res with
    | Error message ->
      Create_view.page
        ~title ~description ~definitions ~verifier ~solution ~public ~message ()
    | Ok _ -> Create_view.ok
  end
  |> Base.html ~title:"Edit" ~login
  |> return_html

end
