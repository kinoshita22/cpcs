open Core.Std
open Misc
module O = Opium.Std

let show = O.get "/" begin fun req ->
  Home_view.page
  |> return_html ~title:"Home" ~login:(Session.get req ~key:"login")
end
