open Core.Std
module O = Opium.Std
open Misc

let list = O.get "/problem/" begin fun req ->
  Problem_model.(find_q (query ~public:true ()))
  |> Problem_view.list
  |> return_html ~title:"Problem list" ~login:(Session.get req ~key:"login")
end

let show = O.get "/problem/:id" begin fun req ->
  let entry = Problem_model.(find_q (query ~id:(O.param req "id") ())) in
  match List.hd entry with
  | None -> client_error `Not_found
  | Some ({ created_by; public; _ } as problem) ->
    let login = Session.get req ~key:"login" in
    if not public && created_by <> (Option.value login ~default:"")
    then client_error `Unauthorized
    else
      Problem_view.page problem ~login
      |> return_html ~title:"Problem" ~login
end

(* solve *)

let check_solution = post_with_query "/problem/:id" begin fun req query ->
  let login = Session.get req ~key:"login" in
  let id = O.param req "id"
  and solution = extract_s query "solution"
  in
  match Problem_model.verify_solution ~id with
  | Result.Error err -> client_error err
  | Ok _ ->
    begin
      match Problem_model.check_solution ~id ~solution with
      | Result.Ok _ ->
        Solution_model.insert ~proof:solution ~problem_id:id ~login;
        Problem_model.verify ~id;
        Problem_view.ok
      | Result.Error err ->
        General.build_error (String.concat ~sep:"\n" err)
    end
    |> return_html ~title:"Problem" ~login
end

(* update *)

let edit_page = O.get "/edit/:id" begin fun req ->
  let login = Session.get req ~key:"login" in
  match Problem_model.validate_edit ~id:(O.param req "id") ~login with
  | Error err -> client_error err
  | Ok { Problem_model.title
       ; description
       ; definitions
       ; verifier
       ; created_by
       ; public
       ; _ } ->
    Create_view.page
      ~title ~description ~definitions ~verifier ~public ()
    |> return_html ~title:"Edit" ~login
end

let edit = post_with_query "/edit/:id" begin fun req query ->
  let login       = Session.get req "login"
  and id          = O.param req "id"
  and title       = extract_s query "title"
  and description = extract_s query "description"
  and definitions = extract_s query "definitions"
  and verifier    = extract_s query "verifier"
  and solution    = extract_s query "solution"
  and public      = extract_s query "public" = "on"
  in
  match Problem_model.validate_edit ~id ~login with
  | Error err -> client_error err
  | Ok    _   ->
    let res =
      Problem_model.edit
        ~id:(O.param req "id")
        ~login ~title ~description ~definitions
        ~verifier ~solution ~public
    in
    begin
      match res with
      | Error message ->
        Create_view.page
          ~title ~description ~definitions ~verifier ~solution ~public ~message ()
      | Ok _ -> Create_view.ok
    end
    |> return_html ~title:"Edit" ~login
end

(* create *)

let create_page = O.get "/create" begin fun req ->
  Create_view.page ()
  |> return_html ~title:"Create" ~login:(Session.get req ~key:"login")
end

let create = post_with_query "/create" begin fun req query ->
  let login = Session.get req ~key:"login" in
  let title       = extract_s query "title"
  and description = extract_s query "description"
  and definitions = extract_s query "definitions"
  and verifier    = extract_s query "verifier"
  and solution    = extract_s query "solution"
  and public      = extract_s query "public" = "on"
  and user        = Option.value login ~default:""
  in
  let res =
    Problem_model.validate
      ~title ~definitions ~verifier ~created_by:user ~solution
  in
  begin
    match res with
    | Ok verified ->
      Problem_model.create
        ~title ~description ~definitions
        ~verifier ~created_by:user ~public
        ~verified;
      Create_view.ok
    | Error message ->
      Create_view.page
        ~title ~description ~definitions ~verifier ~solution ~public ~message ()
  end
  |> return_html ~title:"Create" ~login
end

let c = cascade [list; show; check_solution; edit_page; edit; create_page; create]
