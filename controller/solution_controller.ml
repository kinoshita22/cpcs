open Core.Std
open Misc
module O = Opium.Std

let show_many = O.get "/solution/:id" begin fun req ->
  let login = Session.get req ~key:"login" in
  let problem = Problem_model.(find_q (query ~id:(O.param req "id") ()))
  in
  match List.hd problem with
  | None         -> client_error `Not_found
  | Some problem ->
    if not (Problem_model.visible problem login)
    then Lwt.fail (Client_error `Unauthorized)
    else
      let solutions =
        let open Solution_model in
        List.sort
          ~cmp:(fun { proof = proof1; _} { proof = proof2; _} ->
            compare
              (proof1 |> String.length)
              (proof2 |> String.length))
          (find_q (query ~problem_id:(O.param req "id") ()))
      in
      Solution_view.list problem solutions
      |> return_html ~title:"Solutions" ~login
end

let show = O.get "/solution/:id/:username" begin fun req ->
  let problem  = Problem_model.(find_q (query ~id:(O.param req "id") ())) |> List.hd
  and solution =
    let open Solution_model in
    query
      ~problem_id:(O.param req "id")
      ~username:(O.param req "username")
      ()
    |> find_q
    |> List.hd
  in
  match problem, solution with
  | Some problem, Some solution ->
    Solution_view.show_one problem solution
    |> return_html ~title:"Solution" ~login:(Session.get req ~key:"login")
  | _ -> client_error `Not_found
end

let c = cascade [show_many; show]
