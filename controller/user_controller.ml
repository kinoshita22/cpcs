open Core.Std
open Misc
module O = Opium.Std

let show = O.get "/user/:username" begin fun req ->
  let user = User_model.(find_q (query ~name:(O.param req "username") ())) in
  match List.hd user with
  | None -> client_error `Not_found
  | Some ({ User_model.name; _ } as user) ->
    let created = Problem_model.(find_q (query ~created_by:name ()))
    and solved  = Solution_model.(find_q (query ~username:name ()))
    in
    let login = Session.get req ~key:"login" in
    User_view.page user ~created ~solved
    |> return_html ~title:"User" ~login
end

let c = show
