open Core.Std

let rock_env = match Sys.getenv "ROCK_ENV" with
  | Some "test"        -> `Test
  | Some "development" -> `Development
  | Some "production"  -> `Production
  | _                  -> `Development
