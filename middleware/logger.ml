open Core.Std
open Opium.Std

let logger ?(ch=stderr) () =
  let filter handler req =
    handler req >>= fun res ->
    Cohttp_lwt_body.to_string (Response.body res) >>= fun body ->
    Printf.fprintf ch "[%s] %s \"%s\" %d %d\n"
      Time.(format (now ()) "%F %T %z")
      (Cohttp.Code.string_of_method (Request.meth req))
      (Uri.path_and_query (Request.uri req))
      (Cohttp.Code.code_of_status (Response.code res))
      (String.length body);
    flush ch;
    return res
  in
  Rock.Middleware.create ~filter ~name:(Info.of_string "Logger")
