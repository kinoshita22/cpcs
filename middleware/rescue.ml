open Core.Std
open Opium.Std
open Misc

let error_page status =
  Response.of_string_body ~code:status
    (Printf.sprintf
       "<html><body><h1>%s</h1></body></html>"
       (Cohttp.Code.string_of_status status))

let client_error =
  let filter handler req =
    Lwt.catch
      (fun () -> handler req)
      (function
        | Client_error status ->
          return (error_page status)
        | _exn ->
          Lwt_log.ign_error_f "ERROR: %s" (Exn.to_string _exn);
          return (error_page `Internal_server_error))
  in
  Rock.Middleware.create ~filter ~name:(Info.of_string "Rescue")

let backtrace =
  let filter handler req =
    Lwt.catch
      (fun () -> handler req)
      (fun _exn ->
         let body = (Exn.to_string _exn) ^ "\n" ^ (Exn.backtrace ()) in
         return (Response.of_string_body ~code:`Internal_server_error body))
  in
  Rock.Middleware.create ~name:(Info.of_string "Backtrace") ~filter

