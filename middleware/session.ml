open Core.Std
open Opium.Std

(*
   (* example usage of session *)

let dummy = get "/dummy" begin fun req ->
  let s = Option.value (Session.get req ~key:"count") ~default:"NONE" in
  respond' (`String s)
end

let dummy_post = post "/dummy" begin fun req ->
  match Session.get req ~key:"count" with
  | Some x ->
    Session.set
      req
      (respond (`String x))
      ~key:"count"
      ~data:(string_of_int (int_of_string x + 1))
    |> return
  | None   ->
    Session.set
      req
      (respond (`String "0"))
      ~key:"count"
      ~data:"1"
    |> return
end

*)

let session_key = "rock.session"

let mutex = Mutex.create ()

type session_tbl = string String.Table.t

let pool : session_tbl String.Table.t
  = String.Table.create ~size:10 ()

let dump_pool () =
  String.Table.sexp_of_t (String.Table.sexp_of_t String.sexp_of_t) pool
  |> Sexp.output stderr

let generate_sid ?(len=32) () =
  Random.self_init ();
  String.init len ~f:(fun _ -> Printf.sprintf "%x" (Random.int 16) |> Char.of_string)

let set req resp ~(key : string) ~(data : string) =
  let session = Cookie.get req ~key:session_key in
  let sid =
    match session with
    | Some sid ->
      Mutex.critical_section
        mutex
        ~f:(fun () ->
          match String.Table.find pool sid with
          | Some tbl -> String.Table.set tbl ~key ~data
          | None     ->
            let tbl = String.Table.create ~size:10 () in
            String.Table.set tbl ~key ~data;
            String.Table.set pool ~key:sid ~data:tbl
        );
      sid
    | None ->
      let sid = generate_sid () in
      Mutex.critical_section
        mutex
        ~f:(fun () ->
          let tbl = String.Table.create ~size:10 () in
          String.Table.set tbl ~key ~data;
          String.Table.set pool ~key:sid ~data:tbl
        );
      sid
  in
  Cookie.set resp ~key:session_key ~data:sid

let get req ~(key : string) =
  let (>>=) = Option.(>>=) in
  Cookie.get req ~key:session_key >>= fun sid ->
  Mutex.critical_section
    mutex
    ~f:(fun () ->
      String.Table.find pool sid >>= fun tbl ->
      String.Table.find tbl key)

let destroy req =
  match Cookie.get req ~key:session_key with
  | None     -> ()
  | Some sid ->
    Mutex.critical_section
      mutex
      ~f:(fun () -> String.Table.remove pool sid)

let m =
  let filter handler req =
    let (>>|) = Opium_misc.(>>|) in
    handler req >>| fun resp ->
    match Cookie.get req ~key:session_key with
    | Some sid -> Cookie.set resp ~key:session_key ~data:sid
    | None     -> resp
  in
  Rock.Middleware.create ~filter ~name:(Info.of_string "Session")
