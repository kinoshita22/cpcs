open Core.Std
open Opium.Std

let ($) = Fn.compose
let singleton s = [s]

let rec cascade = function
  | []      -> fun x -> x
  | f :: xs -> fun x -> x |> f |> cascade xs

exception Client_error of Cohttp.Code.status_code
exception Validation_failed of Cow.Xml.t

let client_error status = Lwt.fail (Client_error status)
let validate_fails html = raise (Validation_failed html)

let extract ~default query name =
  try
    List.Assoc.find_exn query name |> List.hd_exn
  with
    Not_found -> default

let extract_s query name = extract ~default:"" query name

let with_query meth uri handler =
  meth uri (fun req -> App.urlencoded_pairs_of_body req >>= handler req)

let get_with_query uri handler = with_query get uri handler
let post_with_query uri handler = with_query post uri handler

let return_html ~title ~login body =
  respond' (`Html (Cow.Html.to_string (Layout.html ~title ~login body)))
