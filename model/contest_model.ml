open Core.Std
open Misc

type t =
  { id           : string
  ; organized_by : string
  ; name         : string
  ; start        : Time.t
  ; finish       : Time.t
  ; problems     : Problem_model.t list
  }

let create_time = Bson.create_string $ Time.to_string

let get_time = Time.of_string $ Bson.get_string

let create_problems problems =
  problems
  |> List.map ~f:(Bson.create_doc_element $ Problem_model.to_bson)
  |> Bson.create_list

let get_problems problems =
  problems
  |> Bson.get_list
  |> List.map ~f:(Problem_model.of_bson $ Bson.get_doc_element)

let to_bson { id
            ; organized_by
            ; name
            ; start
            ; finish
            ; problems } =
  Bson.(
    empty
    |> add_element "id"           (create_string id)
    |> add_element "organized_by" (create_string organized_by)
    |> add_element "name"         (create_string name)
    |> add_element "start"        (create_time start)
    |> add_element "finish"       (create_time finish)
    |> add_element "problems"     (create_problems problems)
  )

let of_bson bson =
  let open Bson in
  { id           = get_element "id"           bson |> get_string
  ; organized_by = get_element "organized_by" bson |> get_string
  ; name         = get_element "name"         bson |> get_string
  ; start        = get_element "start"        bson |> get_time
  ; finish       = get_element "finish"       bson |> get_time
  ; problems     = get_element "problems"     bson |> get_problems
  }

let query
      ?id
      ?organized_by
      ?name
      ?start
      ?finish
      ?problems
      () =
  let open Bson in
  let q = ref empty in
  let add_field key vf = function
    | Some f -> q := add_element key (vf f) !q
    | None   -> ()
  in
  add_field "id"       create_string   id;
  add_field "organized_by" create_string   organized_by;
  add_field "name"     create_string   name;
  add_field "start"    create_time     start;
  add_field "finish"   create_time     finish;
  add_field "problems" create_problems problems;
  !q

let request f = Mongo.create_local_default Config.database "contest" |> f

let find_q query = request begin fun mongo ->
  Mongo.find_q mongo query
  |> MongoReply.get_document_list
  |> List.map ~f:of_bson
end

let parse_problems_exn problems =
  problems
  |> Str.global_replace (Str.regexp " ") ""
  |> String.split ~on:','
  |> List.map ~f:(fun id ->
    try
      List.hd_exn Problem_model.(find_q (query ~id ()))
    with
      Failure "hd" -> failwith ("Problem " ^ id ^ " does not exist.")
  )

let insert ~name ~organized_by ~start ~finish ~problems =
  request begin fun mongo ->
  { id = string_of_int (Counter_model.increment "contest_id")
  ; organized_by
  ; name
  ; start
  ; finish
  ; problems = parse_problems_exn problems
  }
  |> to_bson
  |> singleton
  |> Mongo.insert mongo
end
  
let datetime_pattern =
  Str.regexp "^[0-9][0-9][0-9][0-9]/[0-9][0-9]/[0-9][0-9] [0-9][0-9]:[0-9][0-9]$"

let validate ~login ~name ~start ~finish ~problems =
  if Option.is_none login
  then Error "Login required."
  else if name = "" || start = "" || finish = "" || problems = ""
  then Error "Name, start datetime, finish datetime, and problems must be provided."
  else if not (Str.string_match datetime_pattern start 0)
  then Error "Start datetime is invalid."
  else if not (Str.string_match datetime_pattern finish 0)
  then Error "Finish datetime is invalid."
  else if not Str.(string_match (regexp "^\\([0-9]+\\)\\(,[ ]*[0-9]+\\)*$") problems 0)
  then Error "Problems is invalid."
  else
    try
      ignore (parse_problems_exn problems); Ok ()
    with
      Failure msg -> Error msg
      
let running () =
  let open Bson in
  let now = create_string (Time.to_string (Time.now ())) in
  empty
  |> add_element "start" (create_doc_element (add_element "$lte" now empty))
  |> add_element "finish" (create_doc_element (add_element "$gt" now empty))
  |> find_q

let pending () =
  let open Bson in
  let now = create_string (Time.to_string (Time.now ())) in
  let q =
    try
      add_element
        "start"
        (create_doc_element (add_element "$gt" now empty))
        empty
    with Not_found -> query ()
  in
  find_q q

let get_current_by_id id =
  let c = List.hd (find_q (query ~id ())) in
  match c with
  | None -> None
  | Some ({ start; finish; _ } as c) ->
    let now = Time.now () in
    if start <= now && now < finish
    then Some c
    else None

let verify contest_id problem_id =
  match get_current_by_id contest_id with
  | None -> None
  | Some ({ problems; _ } as contest) ->
    begin
      match List.find ~f:(fun { Problem_model.id; _ } -> id = problem_id) problems with
      | None -> None
      | Some problem -> Some (contest, problem)
    end
