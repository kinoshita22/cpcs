open Core.Std

let request f = Mongo.create_local_default Config.database "counter" |> f

let counter name = Bson.(add_element "_id" (create_string name) empty)

let increment name = request begin fun mongo ->
  (* update *)
  let open Bson in
  let update =
    let inc = add_element "seq" (create_int32 (Int32.of_int_exn 1)) empty in
    add_element "$inc" (create_doc_element inc) empty
  in
  Mongo.update_one ~upsert:true mongo (counter name, update);

  (* current id *)
  Mongo.find_q_one mongo (counter name)
  |> MongoReply.get_document_list
  |> List.hd_exn
  |> get_element "seq"
  |> get_int32
  |> Int32.to_int_exn
end
