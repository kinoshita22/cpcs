open Core.Std
open Misc

type t =
  { id          : string
  ; title       : string
  ; description : string
  ; definitions : string
  ; verifier    : string
  ; created_by  : string
  ; public      : bool
  ; verified    : bool
  }

(* convert Problem_model.t to BSON *)
let to_bson { id
            ; title
            ; description
            ; definitions
            ; verifier
            ; created_by
            ; public
            ; verified } =
  Bson.(
    empty
    |> add_element "id"          (create_string id)
    |> add_element "title"       (create_string title)
    |> add_element "description" (create_string description)
    |> add_element "definitions" (create_string definitions)
    |> add_element "verifier"    (create_string verifier)
    |> add_element "created_by"  (create_string created_by)
    |> add_element "public"      (create_boolean public)
    |> add_element "verified"    (create_boolean verified)
  )

(* convert BSON to Problem_model.t (raise Not_found if any not optional
   fields are missing) *)
let of_bson bson =
  let open Bson in
  let extract vf field = vf (get_element field bson) in
  { id          = extract get_string "id"
  ; title       = extract get_string "title"
  ; description = extract get_string "description"
  ; definitions = extract get_string "definitions"
  ; verifier    = extract get_string "verifier"
  ; created_by  = extract get_string "created_by"
  ; public      = extract get_boolean "public"
  ; verified    = extract get_boolean "verified"
  }

(* BSON object used to query *)
let query
      ?id
      ?title
      ?description
      ?definitions
      ?verifier
      ?created_by
      ?public
      ?verified
      () =
  let open Bson in
  let q = ref empty in
  let add_field key vf = function
    | Some f -> q := add_element key (vf f) !q
    | None   -> ()
  in
  add_field "id"          create_string id;
  add_field "title"       create_string title;
  add_field "description" create_string description;
  add_field "definitions" create_string definitions;
  add_field "verifier"    create_string verifier;
  add_field "created_by"  create_string created_by;
  add_field "public"      create_boolean public;
  add_field "verified"    create_boolean verified;
  !q

let request f = Mongo.create_local_default Config.database "problem" |> f

let find_q query = request begin fun mongo ->
  Mongo.find_q mongo query
  |> MongoReply.get_document_list
  |> List.map ~f:of_bson
end

(* all documents *)
let all () = find_q (query ())

let count () = request begin fun mongo ->
  (* Mongo.count raises Not_found (why?) *)
  Mongo.find mongo
  |> MongoReply.get_document_list
  |> List.length
end

let create
      ~title
      ~description
      ~definitions
      ~verifier
      ~created_by
      ~public
      ~verified = request begin fun mongo ->
  { id = string_of_int (Counter_model.increment "problem_id")
  ; title
  ; description
  ; definitions
  ; verifier
  ; created_by
  ; public
  ; verified
  }
  |> to_bson
  |> singleton
  |> Mongo.insert mongo
end

let update_one ?op (selector, update) =
  let update =
    match op with
    | Some op -> Bson.(add_element op (create_doc_element update) empty)
    | None    -> update
  in
  request (fun mongo -> Mongo.update_one mongo (selector, update))

let validate ~title ~definitions ~verifier ~created_by ~solution =
  if created_by = ""
  then Error (General.error "Login required.")
  else if title = "" || verifier = ""
  then Error (General.error "Title and verifier must be provided.")
  else
  if solution <> ""
  then
    match Build.check ~definitions ~verifier ~solution with
    | Error err -> Error (General.build_error (String.concat ~sep:"\n" err))
    | Ok    _   -> Ok true
  else Ok false

let validate_edit ~id ~login =
  let problem = find_q (query ~id ()) |> List.hd in
  match problem, login with
  | None, _ -> Error `Not_found
  | _, None -> Error `Unauthorized
  | Some ({ created_by; _ } as problem), Some login ->
    if created_by <> login
    then Error `Unauthorized
    else Ok problem

let edit ~id ~login ~title ~description ~definitions ~verifier ~solution ~public =
  let { created_by
      ; verified
      ; definitions = definitions'
      ; verifier = verifier'
      ; _ } = find_q (query ~id ()) |> List.hd_exn in
  let verified = ref verified in
  try
    if title = "" || verifier = ""
    then validate_fails (General.error "Title and verifier must be provided.");

    if (definitions <> definitions' || verifier <> verifier') && solution = ""
    then verified := false;

    if solution <> ""
    then
      begin
        match Build.check ~definitions ~verifier ~solution with
        | Error err -> validate_fails
                         (General.build_error (String.concat ~sep:"\n" err))
        | Ok    _   -> verified := true
      end;
    update_one
      ~op:"$set"
      ( query ~id ()
      , query
          ~title ~description ~definitions ~verifier ~public
          ~verified:(!verified) ());
    Ok []

  with Validation_failed msg -> Error msg

let verify_solution ~id =
  match find_q (query ~id ()) |> List.hd with
  | None                           -> Error `Not_found
  | Some { created_by; public; _ } ->
    if not public then Error `Unauthorized else Ok ()

let check_solution ~id ~solution =
  let { definitions; verifier; _ } = find_q (query ~id ()) |> List.hd_exn in
  Build.check ~definitions ~verifier ~solution

let verify ~id =
  update_one ~op:"$set" (query ~id (), query ~verified:true ())

let visible { created_by; public; _ } login =
  public || created_by = Option.value login ~default:""
