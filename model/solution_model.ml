open Core.Std
open Misc

type t =
  { proof      : string
  ; problem_id : string
  ; username   : string
  ; created_at : Time.t
  }

let to_bson { proof
            ; problem_id
            ; username
            ; created_at } =
  Bson.(
    empty
    |> add_element "proof"      (create_string proof)
    |> add_element "problem_id" (create_string problem_id)
    |> add_element "username"   (create_string username)
    |> add_element "created_at" (create_string (created_at |> Time.to_string))
  )

let of_bson bson =
  let open Bson in
  { proof      = get_string (get_element "proof"      bson)
  ; problem_id = get_string (get_element "problem_id" bson)
  ; username   = get_string (get_element "username"   bson)
  ; created_at = get_string (get_element "created_at" bson) |> Time.of_string
  }

let query ?proof ?problem_id ?username ?created_at () =
  let open Bson in
  let q = ref empty in
  let add_field key vf = function
    | Some f -> q := add_element key (vf f) !q
    | None   -> ()
  in
  add_field "proof"      create_string proof;
  add_field "problem_id" create_string problem_id;
  add_field "username"   create_string username;
  add_field "created_at" (create_string $ Time.to_string) created_at;
  !q

let request f = Mongo.create_local_default Config.database "solution" |> f

let find_q query = request begin fun mongo ->
  Mongo.find_q mongo query
  |> MongoReply.get_document_list
  |> List.map ~f:of_bson
end

let insert ~proof ~problem_id ~login = request begin fun mongo ->
  match login with
  | Some username ->
    Mongo.update_one ~upsert:true mongo
      ( query ~problem_id ~username ()
      , query ~problem_id ~username ~proof ~created_at:(Time.now ()) ()
      )
  | None -> ()
end

