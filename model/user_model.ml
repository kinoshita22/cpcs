open Core.Std

type t = { name : string }

let to_bson { name } =
  Bson.(
    empty
    |> add_element "name"   (create_string name)
  )

let of_bson bson =
  let open Bson in
  { name = get_string (get_element "name" bson) }

let query ?name () =
  Bson.(
    match name with
    | Some name -> add_element "name" (create_string name) empty
    | None      -> empty
  )

let request f = Mongo.create_local_default Config.database "user" |> f

let find_q query = request begin fun mongo ->
  Mongo.find_q mongo query
  |> MongoReply.get_document_list
  |> List.map ~f:of_bson
end

let authenticate username password = request begin fun mongo ->
  let user =
    Mongo.find_q mongo (query ~name:username ())
    |> MongoReply.get_document_list
    |> List.hd
  in
  match user with
  | None      -> false
  | Some user ->
    Bson.(get_string (get_element "password_hash" user))
    |> Bcrypt.hash_of_string
    |> Bcrypt.verify password
end

let insert username password = request begin fun mongo ->
  let hash_str = Bcrypt.(string_of_hash (hash ~count:10 password)) in
  let user =
    Bson.(
      empty
      |> add_element "name"          (create_string username)
      |> add_element "password_hash" (create_string hash_str)
    )
  in
  Mongo.insert mongo [user]
end

let validate username password =
  if username = "" || password = ""
  then Error "Username and password must be provided."
  else if not Str.(string_match (regexp "^[a-z_][a-z0-9_]+$") username 0) ||
          String.length username >= 13
  then Error "Username must be alphanumeric starting with a letter and \
              2-12 characters in length."
  else if Option.is_some (List.hd (find_q (query ~name:username ())))
  then Error "That username is already registered."
  else Ok ()

