open Core.Std
open Opium.Std
open Misc

let _ =

  App.empty

  |> Contest_controller.c
  |> User_controller.c
  |> Auth_controller.c
  |> Solution_controller.c
  |> Problem_controller.c
  |> Home_controller.show

  |> (if Env.rock_env = `Development
      then middleware Rescue.backtrace
      else (fun x -> x))

  |> middleware Rescue.client_error
  |> middleware Session.m
  |> middleware Cookie.m
  |> middleware (Middleware.static ~local_path:"./static" ~uri_prefix:"/static")
  |> middleware (Logger.logger ())

  |> App.cmd_name "cpcs"
  |> App.run_command
