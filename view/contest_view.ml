open Core.Std
open Cow

let page ~login =
  let organize =
    match login with
    | Some _ ->
      <:html<
        <div>
          <span class="u-pull-right"><a href="organize">Organize</a></span>
          <span class="u-cf"></span>
        </div>
      >>
    | None -> []
  in
  let row1 id name organized_by start finish =
    <:html<
      <tr>
        <td><a href="$str:id ^ "/"$">$str:id$</a></td>
        <td><a href="$str:id ^ "/"$">$str:name$</a></td>
        <td><a href="$str:"/user/" ^ organized_by$">$str:organized_by$</a></td>
        <td>$str:Time.format start "%Y-%m-%d %H:%M"$</td>
        <td>$str:Time.format finish "%Y-%m-%d %H:%M"$</td>
      </tr>
    >>
  in
  let row2 id name organized_by start finish =
    <:html<
      <tr>
        <td>$str:id$</td>
        <td>$str:name$</td>
        <td><a href="$str:"/user/" ^ organized_by$">$str:organized_by$</a></td>
        <td>$str:Time.format start "%Y-%m-%d %H:%M"$</td>
        <td>$str:Time.format finish "%Y-%m-%d %H:%M"$</td>
      </tr>
    >>
  in
  let row_f r { Contest_model.id; name; organized_by; start; finish; _ } =
    r id name organized_by start finish
  in
  let table contests r =
    <:html<
    <table class="u-full-width">
      <thead>
        <tr>
          <th>#</th>
          <th>Contest name</th>
          <th>Organized by</th>
          <th>Start</th>
          <th>Finish</th>
        </tr>
      </thead>
      <tbody>
        $list: List.map ~f:(row_f r) contests$
      </tbody>
    </table>
    >>
  in
<:html<
  $organize$

  <h5>Running Contests</h5>

  <div>$table (Contest_model.running ()) row1$</div>

  <h5>Pending Contests</h5>

  <div>$table (Contest_model.pending ()) row2$</div>
>>

let organize
      ?(name="")
      ?(start="")
      ?(finish="")
      ?(problems="")
      ?(message=[])
      ()
  =
  let datetime_pattern = "^[0-9]{4}/[0-9]{2}/[0-9]{2} [0-9]{2}:[0-9]{2}$" in
  let problems_pattern = "^([0-9]+)(,\\s*[0-9]+)*$" in
<:html<
  <div class="organize">

    <div class="message">$message$</div>

    <form method="POST">

      <label for="name">Name</label>
      <input name="name" class="u-full-width" type="text" value="$str:name$" required="true" placeholder="Contest Name"/>

      <label for="start">Start time</label>
      <input name="start" type="text" pattern="$str:datetime_pattern$" value="$str:start$" required="true" placeholder="yyyy/MM/dd HH:mm"/>

      <label for="finish">Finish time</label>
      <input name="finish" type="text" pattern="$str:datetime_pattern$" value="$str:finish$" required="true" placeholder="yyyy/MM/dd HH:mm"/>

      <label for="problems">Problems</label>
      <input name="problems" class="u-full-width" type="text" pattern="$str:problems_pattern$" value="$str:problems$" required="true" placeholder="Comma separated problem id list (e.g. 1,2,10,3)"/>

      <input class="button-primary" type="submit" value="Organize"/>

    </form>

  </div>
>>

let show { Contest_model.id; organized_by; name; start; finish; problems} =
  let list = Problem_view.list ~contest:true problems in
<:html<
  <h5 style="display:inline;">$str:name$</h5>

  <dl>
    <dt>organizer</dt>
    <dd><a href="$str:"/user/" ^ organized_by$">$str:organized_by$</a></dd>
    <dt>start</dt>
    <dd>$str:Time.format start "%Y-%m-%d %H:%M"$</dd>
    <dt>finish</dt>
    <dd>$str:Time.format finish "%Y-%m-%d %H:%M"$</dd>
  </dl>

  $list$
>>
