open Core.Std
open Cow

let page
      ?(title="")
      ?(description="")
      ?(definitions="")
      ?(verifier="")
      ?(solution="")
      ?(public=false)
      ?(message=[])
      ()
  =
  let checkbox =
    if public
    then <:html< <input name="public" type="checkbox" checked="on"/> >>
    else <:html< <input name="public" type="checkbox"/> >>
  in
  <:html<
<div class="create">

  <div class="message">$message$</div>

  <form method="POST">

    <label for="title">Title</label>
    <input name="title" class="u-full-width" type="text" value="$str:title$" required="true"/>

    <label for="desription">Description</label>
    <textarea name="description" class="u-full-width" style="height: 100px" required="true">$str:description$</textarea>

    <label for="definitions">Definitions (optional)</label>
    <textarea name="definitions" class="u-full-width" style="height: 200px">$str:definitions$</textarea>

    <label for="verifier">Verifier</label>
    <textarea name="verifier" class="u-full-width" style="height: 200px" required="true">$str:verifier$</textarea>

    <label for="solution">Solution (optional)</label>
    <textarea name="solution" class="u-full-width" style="height: 200px">$str:solution$</textarea>

    <div>

      <label class="u-pull-right" style="margin-top: 6px;">
        $checkbox$
        <span class="label-body">Make this problem public</span>
      </label>

      <input class="button-primary u-pull-left" type="submit" value="Submit"/>

      <span class="u-cf"></span>

    </div>

  </form>

</div>
>>

let ok = <:html<
  <p>OK!</p>
>>
