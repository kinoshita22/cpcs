open Core.Std
open Cow

let error message = <:html<
  <p class="error">$str:message$</p>
>>

let build_error message = <:html<
  <p class="error">
    Solution is wrong with the error message:
  </p>
  <pre><code>$str:message$</code></pre>
>>
