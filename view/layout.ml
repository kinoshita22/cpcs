open Core.Std
open Cow

let navbar login =
  let right = match login with
    | Some user ->
      <:html<
        <li class="navbar-item">
          <a class="navbar-link" href="$str:"/user/" ^ user$">$str:user$</a>
        </li>
        <li class="navbar-item">
          <a class="navbar-link" href="/logout">logout</a>
        </li>
      >>
    | None ->
      <:html<
        <li class="navbar-item">
          <a class="navbar-link" href="/login">login</a>
        </li>
        <li class="navbar-item">
          <a class="navbar-link" href="/signup">signup</a>
        </li>
      >>
  in
  let create_item = <:html<
    <li class="navbar-item">
      <a class="navbar-link" href="/create">create</a>
    </li>
  >>
  in
  <:html<
  <nav class="navbar">

    <div class="container">

      <ul class="navbar-list u-pull-left">
        <li class="navbar-item">
          <a class="navbar-link" href="/">home</a>
        </li>
        <li class="navbar-item">
          <a class="navbar-link" href="/problem/">problems</a>
        </li>
        <li class="navbar-item">
          <a class="navbar-link" href="/contest/">contest</a>
        </li>
        $if Option.is_some login then create_item else []$
      </ul>

      <ul class="navbar-list u-pull-right">
        $right$
      </ul>

    </div>

 </nav>
>>

let html ~title ~login body = <:html<
  <html>

    <head>

      <meta charset="utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1"/>

      <title>CPCS - $str:title$</title>

      <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css"/>
      <link rel="stylesheet" href="/static/css/normalize.css"/>
      <link rel="stylesheet" href="/static/css/skeleton.css"/>
      <link rel="stylesheet" href="/static/css/custom.css"/>

    </head>

    <body>
      <div class="container">
        $navbar login$
        <div class="contents">
          $body$
        </div>
      </div>
    </body>

  </html>
>>
