open Core.Std
open Cow

let page ?(username="") ?message () =

  let message =
    match message with
    | Some m -> m
    | None   -> []
  in

<:html<
  <div class="message">$message$</div>

  <form method="POST">

    <label for="username">Username</label>
    <input name="username" class="u-full-width" type="text" required="true" value="$str:username$"/>

    <label for="password">Password</label>
    <input name="password" class="u-full-width" type="password" required="true"/>

    <input class="button-primary" type="submit" value="Login"/>

  </form>
>>
