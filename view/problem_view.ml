open Core.Std
open Cow

let list ?(contest=false) problems =
  let problems =
    let row id title created_by verified solved =
      let solution =
        if contest
        then <:html< $str:string_of_int solved$ >>
        else if solved > 0
        then <:html< <a href="$str:"/solution/" ^ id$">$str:string_of_int solved$</a> >>
        else <:html< 0 >>
      in
      <:html<
        <tr>
          <td><a href="$str:id$">$str:id$</a></td>
          <td><a href="$str:id$">$str:title$</a></td>
          <td><a href="$str:"/user/" ^ created_by$">$str:created_by$</a></td>
          <td>$str:if verified then "✔" else "-"$</td>
          <td>$solution$</td>
        </tr>
      >>
    in
    List.map
      ~f:(fun { Problem_model.id; title; created_by; verified; _ } ->
        let solved = Solution_model.(find_q (query ~problem_id:id ()) |> List.length)
        in
        row id title created_by verified solved)
      problems
  in
<:html<
  <table class="u-full-width">
    <thead>
      <tr>
        <th>#</th>
        <th>Title</th>
        <th>Created by</th>
        <th>Verified</th>
        <th>Solved</th>
      </tr>
    </thead>
    <tbody>
      $list:problems$
    </tbody>
  </table>
>>

let page ?message
      { Problem_model.title
      ; description
      ; definitions
      ; verifier
      ; id
      ; created_by
      ; _ }
      ~login
  =

  let message =
    match message with
    | Some m -> <:html< <div class="message">$m$</div> >>
    | None   -> []
  in

  let definitions =
    match definitions with
    | "" -> []
    | _  ->
<:html<
  <h6 class="problem-header">Definitions</h6>
  <pre><code>$str:definitions$</code></pre>
>>
  in

  let edit =
    if Option.value ~default:"" login = created_by
    then <:html<
         <span class="u-pull-right"><a href="$str:"/edit/" ^ id$">edit</a></span>
         >>
    else []
  in

<:html<
  $message$

  <div>
    <h6 class="u-pull-left problem-header problem-title">$str:title$</h6>
    $edit$
    <span class="u-cf"></span>
  </div>

  <h6 class="problem-header">Description</h6>

  <p>$str:description$</p>

  $definitions$

  <h6 class="problem-header">Verifier</h6>

  <pre><code>$str:verifier$</code></pre>

  <h6 class="problem-header">Submit</h6>

  <form method="POST">

    <label for="solution">Your solution</label>
    <textarea name="solution" class="u-full-width" id="solution" style="height: 400px"></textarea>

    <input class="button-primary" type="submit" value="Submit"/>

  </form>
>>

let ok = <:html< <p>OK!</p> >>
