open Core.Std
open Cow

let page ?(username="") ?message () =

  let message =
    match message with
    | Some m -> <:html< <div class="message">$m$</div> >>
    | None   -> []
  in

<:html<
  $message$

  <form method="POST">

    <label for="username">Username</label>
    <input name="username" class="u-full-width" type="text" required="true" value="$str:username$" pattern="^[A-Za-z_][A-Za-z0-9_]{1,11}$" title="must be alphanumeric starting with a letter and 2-12 characters in length"/>

    <label for="password">Password</label>
    <input name="password" class="u-full-width" type="password" required="true"/>

    <input class="button-primary" type="submit" value="Signup"/>

  </form>
>>
