open Core.Std
open Cow

let list problem solutions =
  let { Problem_model.title; id; _ } = problem in

  let solutions =
    let row username size date =
      let user_link =
        <:html< <a href="$str:"/user/" ^ username$">$str:username$</a> >>
      in
      <:html<
      <tr>
        <td>$user_link$</td>
        <td><a href="$str:"/solution/" ^ id ^ "/" ^ username$">$str:size$</a></td>
        <td>$str:date$</td>
      </tr>
    >>
    in
    let open Solution_model in
    List.map
      ~f:(fun { proof; username; created_at; _ } ->
        row
          username
          (String.length proof |> string_of_int)
          (Time.format created_at "%F %T"))
      solutions
  in

<:html<
  <h4>Solutions for <a href="$str:"/problem/" ^ id$">$str:title$</a></h4>
  <table class="u-full-width">
    <thead>
      <tr>
        <th>User</th>
        <th>Proof Size</th>
        <th>Date of Submission</th>
      </tr>
    </thead>
    <tbody>
      $list:solutions$
    </tbody>
  </table>
>>

let show_one
      { Problem_model.title = problem_title; _ }
      { Solution_model.proof; problem_id; username; created_at } =
  let problem_link =
    <:html< <a href="$str:"/problem/" ^ problem_id$">$str:problem_title$</a> >>
  and user_link =
    <:html< <a href="$str:"/user/" ^ username$">$str:username$</a> >>
  in
<:html<
  <h4>Solution for $problem_link$ by $user_link$</h4>
  <pre><code>$str:proof$</code></pre>
  <a href="$str:"/solution/" ^ problem_id$">Back</a>
>>
