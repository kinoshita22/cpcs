open Core.Std
open Cow

let page { User_model.name } ~solved ~created =

  let solved =
    solved
    |> List.map ~f:(fun { Solution_model.problem_id = id; _ } ->
      <:html< <a href="$str:"/problem/" ^ id$">$str:id$</a> >>)
  in

  let created =
    created
    |> List.map ~f:(fun { Problem_model.id; _ } ->
      <:html< <a href="$str:"/problem/" ^ id$">$str:id$</a> >>)
  in

<:html<
  <h4>$str:name$</h4>

  <h6 class="problem-header">solved problems</h6>

  <p>$list:solved$</p>

  <h6 class="problem-header">created problems</h6>

  <p>$list:created$</p>
>>
